import java.net.URLEncoder

def repo = "https://gitlab.com/norashid/chart1.git"
def container_registry = "image-registry.openshift-image-registry.svc:5000"
def proj_name = "microservice"
def namespace = "nodetest"
def appVersion = ""
def lastWorking = "last-working"

pipeline{
		agent{
			label 'helm'
		}

        environment {
            GITLAB_PASSWORD = credentials('gitlab-password')
        }

		stages{

                stage("Package Helm Chart") {
                    steps {
                        echo "Adding helm chart repo from ${repo}  "

                        dir("${env.WORKSPACE}/helm") {
                            checkout([
                                    $class: 'GitSCM',
                                    branches: [[name: "main"]], extensions: [[$class: 'CloneOption', timeout: 20, noTags: false]],
                                    userRemoteConfigs: [[credentialsId: 'git-cloud-arv', url: 'https://gitlab.com/norashid/chart1.git']]
                            ])
                        }

                        // Generate the version semantics
                        script {
                            dir("${env.WORKSPACE}") {
                                def packageJSON = readJSON file: 'package.json'
                                def packageJSONVersion = packageJSON.version

                                appVersion  = "${packageJSONVersion}-${BUILD_NUMBER}"
                                echo "appVersion : ${appVersion}"
                            }
                        }

                        // extract application appPort
                        script {
                            dir("${env.WORKSPACE}/config") {
                                def defaultJSON = readJSON file: 'default.json'
                                appPort = defaultJSON.PORT
                                echo "Application port extracted from default.json : ${appPort}"
                            }
                        }

                        dir("${env.WORKSPACE}/helm") {
                            sh "ls -lrt ${env.WORKSPACE}/helm"

                            // Populate the placeholders
                            script {
                                dir("${env.WORKSPACE}/helm/scripts/ncid-generic") {
//                                     sh "sed -i 's+NCID_IMAGE_NAME+${image_name}+g' values.yaml"
                                    sh "sed -i 's/NCID_APP_PORT/'\"${appPort}\"'/g' values.yaml"
                                    sh "sed -i 's/NCID_NAMESPACE/'\"${proj_name}\"'/g' values.yaml"
                                    sh "cat values.yaml"
                                }

                                try {
                                    dir("${env.WORKSPACE}/helm") {
                                        sh "ls -lrt"

                                        // build deployment yaml
                                        sh "yq eval-all '. as \$item ireduce ({}; . * \$item)' values/common/deployments/common.deployment.yaml values/common/deployments/${proj_name}.common.deployment.yaml > common1.yaml"
                                        sh "cat common1.yaml"
                                        sh "yq eval-all '. as \$item ireduce ({}; . * \$item)' common1.yaml values/dev/deployments/${proj_name}.yaml > common2.yaml"
                                        sh "cat common2.yaml"
                                        sh "yq eval-all '. as \$item ireduce ({}; . * \$item)' scripts/ncid-generic/values.yaml common2.yaml  > final.yaml"
                                        sh "cat final.yaml"

                                        sh "cp final.yaml scripts/ncid-generic/values.yaml"

                                        sh "cat scripts/ncid-generic/values.yaml"
                                    }
                                } catch (Exception e) {
                                    echo 'Error building deployment yaml! Fetching the default values.yaml : ' + e.toString()
                                    sh "cat scripts/ncid-generic/values.yaml"
                                }
                            }

                            dir("${env.WORKSPACE}/helm/scripts") {
                                sh "ls -lrt"
                                sh "./build_chart.sh ${proj_name} ${appVersion}"
                            }
                        }
                    }
                }

                stage("Build and Push Image") {
                    steps {
                        echo "Building image...with version ${appVersion}"
                        script{

                            dir("${env.WORKSPACE}") {
                                if (!fileExists("${env.WORKSPACE}/.npmrc")) {
                                   sh "touch .npmrc"
                                }

                                sh "oc whoami"
                                sh "oc project ${namespace}"

                                sh "oc delete bc ${proj_name} -n ${namespace} || true"
                                sh "oc new-build --name ${proj_name} --binary --strategy docker --to=${container_registry}/${namespace}/${namespace}-${proj_name}:${appVersion} -n ${namespace}"
                                sh "oc start-build --follow ${proj_name} --from-dir=. -n ${namespace}"
                                sh "oc tag ${namespace}-${proj_name}:${appVersion} ${namespace}-${proj_name}:latest -n ${namespace}"

                                sh "oc project ${namespace}"
                            }
                        }
                    }
                }

                stage("Build secrets") {
                    steps{
                        sh "echo building secrets..."

/*
                        // Merging the default.json files
                        dir("${env.WORKSPACE}/helm/values/") {
                            sh "jq -s '.[0] * .[1]' common/configs/${proj_name}.common.default.json ${namespace}/configs/${proj_name}.default.json > default1.json"
                            sh "jq -s '.[0] * .[1]' default1.json ${namespace}/secrets/${proj_name}.secret.json > default.json"
                            sh "cat default.json"
                        }

                        dir("${env.WORKSPACE}/helm/values/") {
//                             sh "oc create configmap ${proj_name}-${namespace}-config --from-file=default.json  -o yaml --dry-run | oc apply -f - -n ${namespace}"

                            // TODO: remove this
                            sh "oc create configmap ${proj_name}-${namespace}-test --from-file=default.json  -o yaml --dry-run | oc apply -f - -n ${namespace}"
                        }
                        */
                    }
                }

                stage("Deploy Dev") {
                    steps {
                        echo "Deploying app to Dev..."

                        dir("${env.WORKSPACE}/helm/scripts") {
                            sh "ls -lrt"

                            // Helm3 Known issue - need to rollback before upgrade (if previous was in error) - https://github.com/helm/helm/issues/8987
                            // TODO: need to improve this and fetch the previous status
                            // sh "helm rollback ${proj_name}-${namespace} -n ${namespace} || true"
//                             sh "helm uninstall ${proj_name} -n ${namespace} || true"
                            script {
                                try {
                                    sh "./deploy_init_chart.sh ${namespace} ${appVersion} ${proj_name} ${container_registry}/${namespace}/${namespace}-${proj_name}"
                                    sh "helm list -a"
                                } catch (Exception e) {
                                    sh "helm rollback ${proj_name} -n ${namespace}"
                                    error('Received exception during deployment....Rolling back ' + e.toString())
                                }
                            }

                        }

                        // TODO: Rollback (to lastWorking version) if error
                    }
                }

        }
}


