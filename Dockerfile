FROM node:14-alpine
WORKDIR /usr/src/app
COPY .npmrc ./
COPY package*.json ./
RUN npm ci --only=production
RUN rm -f .npmrc
COPY . .
EXPOSE 8080
CMD ["npm", "start"]
# add a line for test
