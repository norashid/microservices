/*
 *********************************************************************
 * AI and Robotics Ventures Company Limited Confidential
 *
 * © Copyright AI and Robotics Ventures Company Limited. 2021 All Rights Reserved
 *
 * Users Restricted Rights - Use, duplication, distribution, disclosure are strictly prohibited
 * by AI and Robotics Ventures Company Limited.
 *********************************************************************
*/

'use strict';

const axios = require('axios');
const config = require('../utils/config');
const contentType = 'application/json';
const CONSTANTS = require('../utils/constants');
const { validationResult } = require('express-validator');
const getLogger = require('../utils/logger');
const AdminBFFgateway = require('../gateways/AdminBFFgateway');
const logger = getLogger(module);

class UserProfileController {}

UserProfileController.registerUser = async (req, res, next) => {
    
    const { tracker } = res.locals;

    const adminBFFgateway = new AdminBFFgateway(tracker);
    
    try {
        tracker.outgoing(req, config.USER_PROFILE_SERVICE_URL);

        // Input Data Validation
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            logger.error(`UserProfileController## Error in input data validation` , {
                error: errors.array()
            });
            throw new Error(400);

        }

        // call User Profile Service
        const axiosReq = {
            method: CONSTANTS.AXIOS.POST,
            baseURL: config.USER_PROFILE_SERVICE_URL,
            url: '',
            data: req.body
        };
        const axiosResp = await adminBFFgateway.externalServiceCall(axiosReq);

        tracker.success(req);
        res.status(201).json(axiosResp);


    } catch (err) {

        logger.error(
            'OrgUserServiceController#registerUser - Failed to call UserProfileService',
            {
                error: err.message
            }
        );
        next(err);
    }
};

UserProfileController.getUserById = async (req, res, next) => {
    const { tracker } = res.locals;
    const userId = req.params.userId;
    const adminBFFgateway = new AdminBFFgateway(tracker);
    try {
        tracker.outgoing(req, `${config.USER_PROFILE_SERVICE_URL}/${userId}`);
        const axiosReq = {
            method: CONSTANTS.AXIOS.GET,
            baseURL: config.USER_PROFILE_SERVICE_URL,
            url: `${userId}`,
            data: req.body
        };

        const axiosResp = await adminBFFgateway.externalServiceCall(axiosReq);

        tracker.success(req);
        res.status(200).json(axiosResp);
    } catch (err) {
        logger.error(
            'OrgUserServiceController#getUserById - Failed to call UserProfileService',
            {
                error: err.message
            }
        );
        next(err);
    }
}


UserProfileController.listUsers = async (req, res, next) => {
    const { tracker } = res.locals;
    const adminBFFgateway = new AdminBFFgateway(tracker);


    try {
        tracker.outgoing(req, config.USER_PROFILE_SERVICE_URL);
        const axiosReq = {
            method: CONSTANTS.AXIOS.GET,
            baseURL: config.USER_PROFILE_SERVICE_URL,
            url: '',
            data: req.body
        };

        const axiosResp = await adminBFFgateway.externalServiceCall(axiosReq);
        tracker.success(req);
        res.status(200).json(axiosResp);
    } catch (err) {

        logger.error(
            'OrgUserServiceController#listUsers - Failed to call UserProfileService',
            {
                error: err.message
            }
        );
        next(err);
    }
}


module.exports = UserProfileController;
