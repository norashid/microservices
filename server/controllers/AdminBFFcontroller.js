/*
 **************************************************************************************************
 * AI and Robotics Ventures Company Limited Confidential
 *
 * © Copyright AI and Robotics Ventures Company Limited. 2021 All Rights Reserved
 *
 * Users Restricted Rights - Use, duplication, distribution, disclosure are strictly prohibited
 * by AI and Robotics Ventures Company Limited.
 **************************************************************************************************
*/

'use strict';

const axios = require('axios');
const { validationResult } = require('express-validator');
const config = require('../utils/config');
const CONSTANTS = require('../utils/constants');
const getLogger = require('../utils/logger');
const AdminBFFgateway = require('../gateways/AdminBFFgateway');

const logger = getLogger(module);

class AdminBFFcontroller { }

AdminBFFcontroller.createNewOrg = async (req, res, next) => {
    logger.info('AdminBFFcontroller### createNewOrg ');
    const { tracker } = res.locals;
    const trackerHeader = tracker.header;
    tracker.outgoing(req, config.ORG_PROFILE_SERVICE_BASE_URL + config.ORGANIZATIONS_URL);
    const adminBFFgateway = new AdminBFFgateway(tracker);
    try {
        // Input Data Validation
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            logger.error(`AdminBFFcontroller## Error in input data validation` , {
                error: errors.array()
            });
            throw new Error(400);
        }

        // call Org Profile Service
        const axiosReq = {
            method: CONSTANTS.AXIOS.POST,
            baseURL: config.ORG_PROFILE_SERVICE_BASE_URL,
            url: config.ORGANIZATIONS_URL,
            data: req.body
        };
        const axiosResp = await adminBFFgateway.externalServiceCall(axiosReq);

        tracker.success(req);
        res.status(201).json(axiosResp);

    } catch (err) {
        logger.error('AdminBFFcontroller## createNewOrg',
            {
                error: err.message
            }
        );
        return next(err);
    }
};

AdminBFFcontroller.createCredentialDefinition = async (req, res, next) => {


    logger.info('AdminBFFcontroller### createCredentialDefinition ');
    const { tracker } = res.locals;
    const trackerHeader = tracker.header;
    tracker.outgoing(req, config.ORG_PROFILE_SERVICE_URL);

    const adminBFFgateway = new AdminBFFgateway(tracker);
    try {
        // Input Data Validation
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            logger.error(`AdminBFFcontroller## Error in input data validation` , {

                error: errors.array()
            });
            throw new Error(400);
        }

        // TODO: Call Org Profile Service for validation


        // Call Credential Schema Service
        const axiosReq = {
            method: CONSTANTS.AXIOS.POST,
            baseURL: config.CRED_SCHEMA_SERVICE_BASE_URL,
            url: `${config.CRED_DEFINITION_URL}`,
            data: req.body
        };

        const axiosResp = await adminBFFgateway.externalServiceCall(axiosReq);

        tracker.success(req);
        res.status(201).json(axiosResp);

    } catch (err) {

        logger.error('AdminBFFcontroller## createCredentialDefinition',
            {
                error: err.message
            }
        );
        return next(err);
    }
};

AdminBFFcontroller.getAllCredentialDefinitions = async (req, res, next) => {
    logger.info('AdminBFFcontroller### getAllCredentialDefinitions ');
    const { tracker } = res.locals;
    const trackerHeader = tracker.header;
    tracker.outgoing(req, config.CRED_SCHEMA_SERVICE_BASE_URL);
    const adminBFFgateway = new AdminBFFgateway(tracker);
    try {

        // call credential Service
        const axiosReq = {
            method: CONSTANTS.AXIOS.GET,
            baseURL: config.CRED_SCHEMA_SERVICE_BASE_URL,
            url: `${config.CRED_DEFINITION_URL}`
        };



        const axiosResp = await adminBFFgateway.externalServiceCall(axiosReq);

        tracker.success(req);
        res.status(200).json(axiosResp);

    } catch (err) {
        logger.error('AdminBFFcontroller## getAllCredentialDefinitions',
            {
                error: err.message
            }
        );
        return next(err);
    }
};


AdminBFFcontroller.getAllCredDefByWalletAgentId = async (req, res, next) => {
    logger.info('AdminBFFcontroller### getAllCredDefByWalletAgentId ');
    const { tracker } = res.locals;
    const trackerHeader = tracker.header;
    tracker.outgoing(req, config.CRED_SCHEMA_SERVICE_BASE_URL);
    const adminBFFgateway = new AdminBFFgateway(tracker);
    try {

        // call credential Service
        const axiosReq = {
            method: CONSTANTS.AXIOS.GET,
            baseURL: config.CRED_SCHEMA_SERVICE_BASE_URL,
            url: `${config.CRED_DEFINITION_URL}/${req.params.walletAgentId}`
        };



        const axiosResp = await adminBFFgateway.externalServiceCall(axiosReq);

        tracker.success(req);
        res.status(200).json(axiosResp);

    } catch (err) {
        logger.error('AdminBFFcontroller## getAllCredentialDefinitions',
            {
                error: err.message
            }
        );
        return next(err);
    }
};


AdminBFFcontroller.getCredentialDefDetails = async (req, res, next) => {
    logger.info('AdminBFFcontroller### getCredentialDefDetails ');
    const { tracker } = res.locals;
    const trackerHeader = tracker.header;
    tracker.outgoing(req, config.CRED_SCHEMA_SERVICE_BASE_URL);
    const adminBFFgateway = new AdminBFFgateway(tracker);
    try {

        // call credential Service
        const axiosReq = {
            method: CONSTANTS.AXIOS.GET,
            baseURL: config.CRED_SCHEMA_SERVICE_BASE_URL,
            url: `${config.CRED_DEFINITION_URL}/${req.params.credentialDefinitionId}`
        };



        const axiosResp = await adminBFFgateway.externalServiceCall(axiosReq);

        tracker.success(req);
        res.status(200).json(axiosResp);

    } catch (err) {
        logger.error('AdminBFFcontroller## getCredentialDefDetails',

            {
                error: err.message
            }
        );
        return next(err);
    }
};


module.exports = AdminBFFcontroller;