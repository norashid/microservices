/*

 * AI and Robotics Ventures Company Limited Confidential
 *
 * © Copyright AI and Robotics Ventures Company Limited. 2021 All Rights Reserved
 *
 * Users Restricted Rights - Use, duplication, distribution, disclosure are strictly prohibited
 * by AI and Robotics Ventures Company Limited.

*/

// import dependencies and initialize express
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const cors = require('cors');
const routes = require('./routes');

const RequestTracker = require('./utils/RequestTracker');
const ErrorHandler = require('./utils/ErrorHandler');
const config = require('./utils/config');
const useAppSec = require('./utils/appSec');

useAppSec(app);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: '1mb' }));

app.use(RequestTracker.tracker);
app.use(routes);
app.use(ErrorHandler);

app.use((_, res) => {
    res.status(404).json({
        message: 'Not Found'
    });
});

module.exports = app;
