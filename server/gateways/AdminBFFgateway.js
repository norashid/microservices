/*
 **************************************************************************************************
 * AI and Robotics Ventures Company Limited Confidential
 *
 * © Copyright AI and Robotics Ventures Company Limited. 2021 All Rights Reserved
 *
 * Users Restricted Rights - Use, duplication, distribution, disclosure are strictly prohibited
 * by AI and Robotics Ventures Company Limited.
 **************************************************************************************************
*/

const axios = require('axios');
const config = require('../utils/config');
const getLogger = require('../utils/logger');

const logger = getLogger(module);

class AdminBFFgateway {
    constructor(tracker) {
        this.tracker = tracker;
        const trackerHeader = tracker.header;
    }

    async externalServiceCall(axiosReq) {
        logger.info('AdminBFFgateway ##externalServiceCall');
        
        // make axios call and return response if proper data is received
        const axiosResp = await axios(axiosReq);
        if (!axiosResp?.data) {
            logger.error('AdminBFFgateway### externalServiceCall - No data received from external service');
            throw new Error(404);
        }
        return axiosResp.data;
    }

}

module.exports = AdminBFFgateway;
