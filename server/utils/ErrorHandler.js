/* eslint-disable no-unused-vars */

const RequestTracker = require('./RequestTracker');

const errorMessages = {
	400: { responseCode: 400, message: "Missing or Invalid data" },
	401: { responseCode: 401, message: "Authorization Failed" },
	404: { responseCode: 404, message: "No data found" },
	500: { responseCode: 500, message: "Internal server error" },
};

/**
 * Handle axios error types
 * 
 * @param {object} error AxiosError
 * @param {object} res Express response
 * @returns {object} status
 */
function handleAxiosError(error, res) {
	if (error.response) {
		/**
		 * Request was made and server responded with status code
		 * that falls out of range of 2xx. Returns target request
		 * status and data
		 */
		const { data } = error.response;
		return {
			statusCode: error.response.status,
			response: {
				message: error.message, ...data
			}
		}
	} else if (error.request) {
		/**
		 * Request was made but no response was received. Returns
		 * 424 Failed Dependency with error message
		 */
		return {
			statusCode: 424,
			response: {
				message: error.message
			}
		}
	}
}

const ErrorController = (error, req, res, next) => {
	let statusCode;
	let response;

	let tracker = res.locals.tracker;
	if (!tracker) {
		tracker = new RequestTracker();
	}

	if (error.response) {
		/**
		 * If error is AxiosError
		 */
		const axiosError = handleAxiosError(error, res);
		statusCode = axiosError.statusCode;
		response = axiosError.response;
	} else if (error.statusCode) {
		/**
		 * If error is internal error
		 */
		statusCode = error.statusCode;
		response = {
			message: error.message
		}
	} else {
		// Get the thrown status code 
		statusCode = error.message;
		response = errorMessages[statusCode]
	}

	tracker.error(req);
	res.status(statusCode).json(response);
};

module.exports = ErrorController;
