/*
 **************************************************************************************************
 * AI and Robotics Ventures Company Limited Confidential
 *
 * © Copyright AI and Robotics Ventures Company Limited. 2021 All Rights Reserved
 *
 * Users Restricted Rights - Use, duplication, distribution, disclosure are strictly prohibited
 * by AI and Robotics Ventures Company Limited.
 **************************************************************************************************
*/

const { format, createLogger, transports } = require('winston');
const config = require('./config');
const yn = require('yn');
const { cloneDeep } = require('lodash');
const { combine, timestamp, label, printf, colorize, json } = format;

const loggerOptions = {
    level: config.LOGGER.LEVEL || 'info',
    silent: yn(config.LOGGER.SILENT) || false,
    json: yn(config.LOGGER.JSON) || false,
    appName: config.APP_NAME || ''
};


const myFormat = printf(({ level, message, label, timestamp, ...rest }) => {
    return `${timestamp} [${label}] ${level}: ${message} ${JSON.stringify(rest)}`;
});

/**
 * Format logger
 * 
 * @returns {object} logger formats
 */
function getLoggerFormats() {
    const formats = [
        label({ label: loggerOptions.appName }),
        timestamp(),
        //   redact()
    ];
    if (loggerOptions.json) {
        formats.push(json());
    } else {
        formats.push(colorize(), myFormat);
    }
    return formats;
}

const transport = {
    console: new transports.Console()
};

const logger = createLogger({
    level: loggerOptions.level,
    silent: loggerOptions.silent,
    format: combine(...getLoggerFormats()),
    transports: [
        transport.console
    ]
});

const getLogger = (module, metadata = {}) => {
    const filename = module.filename.split('/').slice(-2).join('/');
    return logger.child({
        filename, ...metadata
    });
};

module.exports = getLogger;