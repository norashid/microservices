const getLogger = require('./logger');
const logger = getLogger(module)


exports.POSTGRES_CHECKS = {
    livenessCheck: async ({ sequelize }) => {
        logger.silly('core#postgres liveness check')
        await sequelize.authenticate()
        return 'ok'
    },
    readinessCheck: () => {},
    cleanup: async ({ sequelize }) => {
        logger.debug('core#postgres cleanup')
        await sequelize.close()
        return 'ok'
    },
    shutdown: () => {}
}