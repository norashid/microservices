const getLogger = require('./logger');
const { v4: uuidv4 } = require('uuid');

const logger = getLogger(module);

class RequestTracker {
    constructor(requestId) {
        this.requestId = requestId ? requestId : uuidv4();
        this.headerKey = 'Request-ID'
    }

    get header() {
        return {
            [this.headerKey]: this.requestId
        }
    }

    incoming(req, metadata = {}) {
        const { originalUrl, url, method, params } = req; 
        logger.info('>>>> Request incoming', {
            requestId: this.requestId, url: (originalUrl || url || req), method, params, metadata
        })
    }

    error(req, metadata = {}) {
        const { originalUrl, url, method, params } = req;
        logger.error('<<<< Request error', {
            requestId: this.requestId, url: (originalUrl || url || req), method, params, metadata
        })
    }

    success(req, metadata = {}) {
        const { originalUrl, url, method, params } = req;
        logger.debug('<<<< Request success', {
            requestId: this.requestId, url: (originalUrl || url || req), method, params, metadata
        })
    }

    outgoing(req, metadata = {}) {
        const { originalUrl, url, method } = req;
        logger.debug('>>>> Request outgoing', {
            requestId: this.requestId, 
            url: (originalUrl || url || req), method, metadata
        });
    }
}

RequestTracker.tracker = (req, res, next) => {
    const requestId = req.header('Request-ID');
    const tracker = new RequestTracker(requestId);
    res.locals.tracker = tracker;
    tracker.incoming(req);
    next();
}

module.exports = RequestTracker;