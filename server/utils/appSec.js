const helmet = require('helmet');
const getLogger = require('./logger');
const logger = getLogger(module);
const cors = require('cors');

const useAppSec = (app, options = {}) => {
    const defaultSecOptions = {
        hsts: false
    }
    const helmetOptions = Object.assign({}, defaultSecOptions, options.helmet)
    logger.debug('core#appSec putting on secure helmet', { options: helmetOptions })
    app.use(helmet(helmetOptions));
    
    const corsOptions = Object.assign({}, options.cors)
    logger.debug('core#appSec setting cors', { options: corsOptions })
    app.use(cors(corsOptions));
}

module.exports = useAppSec