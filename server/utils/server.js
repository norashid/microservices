/* eslint-disable no-undef */
const http = require('http');
const { createTerminus, HealthCheckError } = require('@godaddy/terminus');
const getLogger = require('./logger');
const { POSTGRES_CHECKS } = require('./healthChecks')

const logger = getLogger(module);

/**
 * Type definitions
 * 
 * @typedef {object} HealthCheckOptions
 * @property {string} [livenessEndpoint] Endpoint to use for liveness check, defaults to _/health/liveness
 * @property {string} [readinessEndpoint] Endpoint to use for readiness check, defaults to _/health/readiness
 * @property {string} [timeout] number of milliseconds before forceful exiting, defaults to 3000
 * @property {Array<Dependency>} [dependencies] array of dependencies
 * @property {Function} [shutdown] callback for shutdown check
 * @property {Function} [cleanup] callback for cleanup check
 * 
 * @typedef {object} Dependency
 * @property {string} name name of the dependency
 * @property {object} config additional configuration related to dependency} param0 
 */

const DEPENDENCY_MAP = {
    postgres: POSTGRES_CHECKS
}

/**
 * 
 * @param {object} options - options
 * @param {Array<Dependency>} options.dependencies - app dependencies
 * @returns {object} check result 
 */
const livenessCheck = ({ dependencies = [] }) => {
    return async () => {
        logger.silly(`core#livenessCheck starting`)
        const result = {}
        let hasError = false;

        for (const dependency of dependencies) {
            try {
                const health = await DEPENDENCY_MAP[dependency.name].livenessCheck(dependency.config)
                result[dependency.name] = health;
            } catch (err) {
                hasError = true;
                logger.error(`core#livenessCheck failed for ${dependency.name}`, { error: err.message })
                result[dependency.name] = err.message;
            }
        }
        
        if (hasError) {
            logger.error(`core#livenessCheck failed`, result)
            throw new HealthCheckError(`core#livenessCheck failed`, result);
        }
        
        logger.silly(`core#livenessCheck complete`)
        return result
    }
}

/**
 * 
 * @param {object} options - options
 * @param {Array<Dependency>} options.dependencies - app dependencies
 * @returns {object} check result 
 */
const readinessCheck = ({ dependencies = [] }) => {
    return async () => {
        logger.silly(`core#readinessCheck starting`)
        const result = {}
        let hasError = false;
        
        for (const dependency of dependencies) {
            try {
                const health = await DEPENDENCY_MAP[dependency.name].readinessCheck(dependency.config)
                result[dependency.name] = health;
            } catch (err) {
                logger.error(`core#readinessCheck failed for ${dependency.name}`, { error: err.message })
                hasError = true;
                result[dependency.name] = err.message;
            }
        }

        if (hasError) {
            logger.error(`core#readinessCheck failed`, result)
            throw new HealthCheckError(`core#readinessCheck failed`, result);
        }
        logger.silly(`core#readinessCheck complete`)
        return Promise.resolve(result)
    }
}

/**
 * 
 * @param {object} options - options
 * @param {Array<Dependency>} options.dependencies - app dependencies
 * @param {Function} options.cleanup - callback function
 * @returns {object} check result 
 */
const cleanup = ({ dependencies = [], cleanup = () => {} }) => {
    return async () => {
        logger.debug(`core#cleanup starting`)
        for (const dependency of dependencies) {
            await DEPENDENCY_MAP[dependency.name].cleanup(dependency.config)
        }
        await cleanup()
        return { cleanup: 'ok' }
    }
}

/**
 * 
 * @param {object} options - options
 * @param {Array<Dependency>} options.dependencies - app dependencies
 * @param {Function} options.shutdown - callback function
 * @returns {object} check result 
 */
const shutdown = ({ dependencies = [], shutdown = () => {} }) => {
    return async () => {
        logger.debug(`core#shutdown starting`)
        for (const dependency of dependencies) {
            await DEPENDENCY_MAP[dependency.name].shutdown(dependency.config)
        }
        await shutdown()
        return { shutdown: 'ok' }
    }
}


/**
 * 
 * @param {*} server Express app server
 * @param {*} options HealthCheckOptions
 * 
 */
function healthCheck(server, options) {
    logger.debug(`core#healthCheck configuring health check endpoints`)
    const livenessEndpoint = options.livenessEndpoint || '/_health/liveness';
    const readinessEndpoint = options.readinessEndpoint || '/_health/readiness';
    const terminusOptions = {
        healthChecks: {
            [livenessEndpoint]: livenessCheck(options),
            [readinessEndpoint]: readinessCheck(options)
        },
        timeout: options.timeout || 3000,
        signals: ['SIGINT', 'SIGTERM'],
        onSignal: cleanup(options),
        onShutdown: shutdown(options),
        beforeShutdown: () => {
            return new Promise(resolve => {
                setTimeout(resolve, 5000)
            })
        },
        logger: (msg, err) => {
            if (err) {
                logger.error(`core#logger ${msg}`, { error: err })
            } else {
                logger.debug(`core#logger ${msg}`)
            }
        }
    }
    createTerminus(server, terminusOptions);
    logger.info(`core#healthCheck health check liveness endpoints: ${livenessEndpoint}`)
    logger.info(`core#healthCheck health check readiness endpoints: ${readinessEndpoint}`)
}

const startServer = ({ app, port, options }) => {
    logger.debug(`core#startServer starting server on ${port}`)
    const server = http.createServer(app)
    
    healthCheck(server, options)
    
    server.listen(port)
    logger.info(`core#startServer listening on ${port}`)
};

module.exports = startServer;