/*
 **************************************************************************************************
 * AI and Robotics Ventures Company Limited Confidential
 *
 * © Copyright AI and Robotics Ventures Company Limited. 2021 All Rights Reserved
 *
 * Users Restricted Rights - Use, duplication, distribution, disclosure are strictly prohibited
 * by AI and Robotics Ventures Company Limited.
 **************************************************************************************************
*/

const config = require('./config');
const AdminBFFgateway = require('../gateways/AdminBFFgateway');
const CONSTANTS = require('./constants');
const getLogger = require('./logger');

const logger = getLogger(module);

const AuthHandler = async (req, res, next) => {
    logger.info('Authorization Handler Middleware');
    try {
        const { tracker } = res.locals;
        const trackerHeader = tracker.header;
        tracker.outgoing(req, config.APPID_SERVICE_BASE_URL);
        const adminBFFgateway = new AdminBFFgateway(tracker);

        // Get token from request header
        const token = req.header('authorization').split(' ');

        if (token[0] !== 'Bearer') {
            logger.error('Authorization Failed :: Bearer token not found');
            throw new Error(401);
        }
        const data = {
            token: token[1]
        };

        // Validate token via AppID service 
        const axiosReq = {
            method: CONSTANTS.AXIOS.POST,
            baseURL: config.APPID_SERVICE_BASE_URL,
            url: config.APPID_SERVICE_VERIFY_TOKEN_URL,
            data
        };
        const axiosResp = await adminBFFgateway.externalServiceCall(axiosReq);

        // If validation failed invoke ErrorHandler
        if (!axiosResp.tokenActive) {
            logger.error('Authorization Middleware - Token validation failed');
            throw new Error(401);
        }
        next();
    } catch (err) {
        logger.error('Authorization Failed :: Invalid Auth token',
            {
                error: err.message
            }
        );
        return next(err);
    }
}

module.exports = AuthHandler;