const CONSTANTS = {
    AXIOS: {
        GET: "GET",
        POST: "POST",
        PUT: "PUT",
        DELETE: "DELETE",
        CONTENT_TYPE_JSON: "application/json"
    }
};

module.exports = CONSTANTS;