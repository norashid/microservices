/*
 *********************************************************************
 * AI and Robotics Ventures Company Limited Confidential
 *
 * © Copyright AI and Robotics Ventures Company Limited. 2021 All Rights Reserved
 *
 * Users Restricted Rights - Use, duplication, distribution, disclosure are strictly prohibited
 * by AI and Robotics Ventures Company Limited.
 *********************************************************************
*/

require('dotenv').config();

const getLogger = require('./utils/logger');
const startServer = require('./utils/server');
const config = require('./utils/config');
const logger = getLogger(module);

const app = require('./server');

const port = config.PORT;

const options = {
    dependencies: []
};

/**
 * start application
 */
async function start() {
    startServer({ app, port, options });
}

start()
    .then(() => logger.info('app started successfully'))
    .catch(() => logger.error('app failed to start'));
