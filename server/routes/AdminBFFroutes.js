/*
 **************************************************************************************************
 * AI and Robotics Ventures Company Limited Confidential
 *
 * © Copyright AI and Robotics Ventures Company Limited. 2021 All Rights Reserved
 *
 * Users Restricted Rights - Use, duplication, distribution, disclosure are strictly prohibited
 * by AI and Robotics Ventures Company Limited.
 **************************************************************************************************
*/

const express = require('express');
const { body } = require('express-validator');
const config = require('../utils/config');
const AdminBFFcontroller = require('../controllers/AdminBFFcontroller');
const router = express.Router();

const orgEndpointV1 = config.API_BASE + config.API_VERSION.V1 + config.ORGANIZATIONS_URL;
const credDefinitionEndpoint = config.API_ONLY + config.ORGANIZATIONS_URL + config.CRED_DEFINITION_URL;
const adminCredDefinitionsEndpoint = config.ADMIN_ONLY + config.API_VERSION.V1 + config.ORGANIZATIONS_URL


// create New Organization endpoint
router.post(orgEndpointV1,
    [body('emailId').isEmail(),
    body('roles').exists({ checkFalsy: true }),
    body('name').exists({ checkFalsy: true }),
    body('corpAdmin').exists({ checkFalsy: true })],
    AdminBFFcontroller.createNewOrg
);


router.post(credDefinitionEndpoint,
    [
        body('schemaId').exists({ checkFalsy: true }),
        body('payloadFormat').exists({ checkFalsy: true }),
        body('credentialType').exists({ checkFalsy: true }),
        body('walletAgentId').exists({ checkFalsy: true })
    ],
    AdminBFFcontroller.createCredentialDefinition
);

router.get(adminCredDefinitionsEndpoint + config.CRED_DEFINITION_URL, AdminBFFcontroller.getAllCredentialDefinitions);

router.get(adminCredDefinitionsEndpoint + config.CRED_DEFINITION_URL + '/:credentialDefinitionId' , AdminBFFcontroller.getCredentialDefDetails);



module.exports = router;
