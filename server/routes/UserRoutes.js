
/*
 *********************************************************************
 * AI and Robotics Ventures Company Limited Confidential
 *
 * © Copyright AI and Robotics Ventures Company Limited. 2021 All Rights Reserved
 *
 * Users Restricted Rights - Use, duplication, distribution, disclosure are strictly prohibited
 * by AI and Robotics Ventures Company Limited.
 *********************************************************************
*/


const express = require('express');
const config = require('../utils/config');
const UserProfileController = require('../controllers/UserProfileController');
const router = express.Router();

const { body, param } = require('express-validator');


/**
 * /users
 */
 const userEndpointV1 = config.API_BASE + config.API_VERSION.V1 + '/users';

 router.post(
    userEndpointV1,
    [
     body('email').isEmail(),
     body('userName').exists({checkFalsy: true}),
     body('phone').exists({checkFalsy: true}),
     body('orgDid').exists({checkFalsy: true}),
     body('role').exists({checkFalsy: true})],
    UserProfileController.registerUser
 );

 router.get(
    userEndpointV1 + '/:userId',
    UserProfileController.getUserById
 );


 router.get(
    userEndpointV1,
    UserProfileController.listUsers
 );
 

module.exports = router;
