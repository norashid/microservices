/*
 *********************************************************************
 * AI and Robotics Ventures Company Limited Confidential
 *
 * © Copyright AI and Robotics Ventures Company Limited. 2021 All Rights Reserved
 *
 * Users Restricted Rights - Use, duplication, distribution, disclosure are strictly prohibited
 * by AI and Robotics Ventures Company Limited.
 *********************************************************************
*/

const express = require('express');
const router = express.Router();
const AdminBffRoutes = require('./AdminBFFroutes');
const UserRoutes = require('./UserRoutes');

router.use(AdminBffRoutes);
router.use(UserRoutes);

module.exports = router;
