/*
 **************************************************************************************************
 * AI and Robotics Ventures Company Limited Confidential
 *
 * © Copyright AI and Robotics Ventures Company Limited. 2021 All Rights Reserved
 *
 * Users Restricted Rights - Use, duplication, distribution, disclosure are strictly prohibited
 * by AI and Robotics Ventures Company Limited.
 **************************************************************************************************
*/

const axios = require('axios');
const request = require('supertest');
const app = require('../server/server');
const config = require('../server/utils/config');
const {
    authToken,
    createOrgUserReq,
    createOrgUserRes,
    listOrderUserRes,
    getUserReq,
    getUserRes
} = require('./testdata');

jest.mock('axios');

describe('Admin BFF User Profile Service Test Suite', () => {

    test('Valid Request: POST admin/v1/users', async () => {
        const endpoint = config.API_BASE + '/v1/users';
        const axiosResp = { data: createOrgUserRes };
        const tokenResp = {
            data: {
                tokenActive: true
            }
        };

        axios.mockImplementation((axiosReq) => {
            if (axiosReq.url === '/verifyUserToken') {
                return Promise.resolve(tokenResp);
            } else {
                return Promise.resolve(axiosResp);
            }
        });

        await request(app).post(endpoint).set('Authorization', authToken).send(createOrgUserReq).expect(201);
    });


    test('Valid Request: GET admin/v1/users', async () => {
        const endpoint = config.API_BASE + '/v1/users';
        const axiosResp = { data: listOrderUserRes };
        const tokenResp = {
            data: {
                tokenActive: true
            }
        };

        axios.mockImplementation((axiosReq) => {
            if (axiosReq.url === '/verifyUserToken') {
                return Promise.resolve(tokenResp);
            } else {
                return Promise.resolve(axiosResp);
            }
        });

        await request(app).get(endpoint).set('Authorization', authToken).expect(200);
    });

    test('Valid Request: GET admin/v1/users/:userId', async () => {

        const endpoint = config.API_BASE + '/v1/users/' + getUserReq.params.userId;
        const axiosResp = { data: getUserRes };
        const tokenResp = {
            data: {
                tokenActive: true
            }
        };

        axios.mockImplementation((axiosReq) => {
            if (axiosReq.url === '/verifyUserToken') {
                return Promise.resolve(tokenResp);
            } else {
                return Promise.resolve(axiosResp);
            }
        });

        await request(app).get(endpoint).set('Authorization', authToken).expect(200);
    });

});

