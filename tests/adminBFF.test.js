/*
 **************************************************************************************************
 * AI and Robotics Ventures Company Limited Confidential
 *
 * © Copyright AI and Robotics Ventures Company Limited. 2021 All Rights Reserved
 *
 * Users Restricted Rights - Use, duplication, distribution, disclosure are strictly prohibited
 * by AI and Robotics Ventures Company Limited.
 **************************************************************************************************
*/

const axios = require('axios');
const request = require('supertest');
const app = require('../server/server');
const config = require('../server/utils/config');
const {
    createOrgReq,
    createOrgRes,
    authToken,
    createOrg_invalidReq,
    createCredDefReq,
    invalidCreateCredDef,
    getAllCredDefRes,
    getCredDefReq,
    getCredDefRes,
    inCompleteCreateCredDef,
} = require('./testdata');

jest.mock('axios');

describe('Admin BFF Test Suite', () => {

    test('Valid Request: POST admin/v1/organizations', async () => {
        const endpoint = config.API_BASE + '/v1/organizations';
        const axiosResp = { data: createOrgRes };
        const tokenResp = {
            data: {
                tokenActive: true
            }
        };

        axios.mockImplementation((axiosReq) => {
            if (axiosReq.url === '/verifyUserToken') {
                return Promise.resolve(tokenResp);
            } else {
                return Promise.resolve(axiosResp);
            }
        });

        await request(app).post(endpoint)
            .set('Authorization', authToken)
            .send(createOrgReq)
            .expect(201);
    });


    test('Invalid Request: POST admin/v1/organizations', async () => {
        const endpoint = config.API_BASE + '/v1/organizations';
        const axiosResp = { data: createOrgRes };
        const tokenResp = {
            data: {
                tokenActive: true
            }
        };

        axios.mockImplementation((axiosReq) => {
            if (axiosReq.url === '/verifyUserToken') {
                return Promise.resolve(tokenResp);
            } else {
                return Promise.resolve(axiosResp);
            }
        });

        await request(app).post(endpoint)
            .set('Authorization', authToken)
            .send(createOrg_invalidReq)
            .expect(400);

    });

    test('No Data Found: POST admin/v1/organizations', async () => {
        const endpoint = config.API_BASE + '/v1/organizations';
        const axiosResp = { data: '' };
        const tokenResp = {
            data: {
                tokenActive: true
            }
        };

        axios.mockImplementation((axiosReq) => {
            if (axiosReq.url === '/verifyUserToken') {
                return Promise.resolve(tokenResp);
            } else {
                return Promise.resolve('');
            }
        });

        await request(app).post(endpoint)
            .set('Authorization', authToken)
            .send(createOrgReq)
            .expect(404);
    });


    test('Valid Request: POST /api/organizations/definitions', async () => {
        const endpoint = config.API_ONLY + '/organizations' + config.CRED_DEFINITION_URL;
        const axiosResp = { data: createCredDefReq };
        const tokenResp = {
            data: {
                tokenActive: true
            }
        };

        axios.mockImplementation((axiosReq) => {
            if (axiosReq.url === '/verifyUserToken') {
                return Promise.resolve(tokenResp);
            } else {
                return Promise.resolve(axiosResp);
            }
        });

        await request(app).post(endpoint).set('Authorization', authToken).send(createCredDefReq).expect(201);
    });

    test('Invalid Request: POST /api/organizations/definitions', async () => {
        const endpoint = config.API_ONLY + config.ORGANIZATIONS_URL + config.CRED_DEFINITION_URL;
        const axiosResp = { data: '' };
        const tokenResp = {
            data: {
                tokenActive: true
            }
        };

        axios.mockImplementation((axiosReq) => {
            if (axiosReq.url === '/verifyUserToken') {
                return Promise.resolve(tokenResp);
            } else {
                return Promise.resolve(axiosResp);
            }
        });

        await request(app).post(endpoint).set('Authorization', authToken).send(inCompleteCreateCredDef).expect(400);
    });

    test('Valid Request: GET /organizations/definitions', async () => {
        const endpoint = config.ADMIN_ONLY + config.API_VERSION.V1 + config.ORGANIZATIONS_URL + config.CRED_DEFINITION_URL;
        const axiosResp = { data: getAllCredDefRes };
        const tokenResp = {
            data: {
                tokenActive: true
            }
        };

        axios.mockImplementation((axiosReq) => {
            if (axiosReq.url === '/verifyUserToken') {
                return Promise.resolve(tokenResp);
            } else {
                return Promise.resolve(axiosResp);
            }
        });

        await request(app).get(endpoint).set('Authorization', authToken).expect(200);
    });

    test('Valid Request: GET /organizations/definitions/:credentialDefinitionId', async () => {
        const endpoint = config.ADMIN_ONLY + config.API_VERSION.V1 + config.ORGANIZATIONS_URL  + config.CRED_DEFINITION_URL + `/${getCredDefReq.params.credentialDefinitionId}`;
        const axiosResp = { data: getCredDefRes };
        const tokenResp = {
            data: {
                tokenActive: true
            }
        };

        axios.mockImplementation((axiosReq) => {
            if (axiosReq.url === '/verifyUserToken') {
                return Promise.resolve(tokenResp);
            } else {
                return Promise.resolve(axiosResp);
            }
        });

        await request(app).get(endpoint).set('Authorization', authToken).expect(200);
    });

});

