/*
 **************************************************************************************************
 * AI and Robotics Ventures Company Limited Confidential
 *
 * © Copyright AI and Robotics Ventures Company Limited. 2021 All Rights Reserved
 *
 * Users Restricted Rights - Use, duplication, distribution, disclosure are strictly prohibited
 * by AI and Robotics Ventures Company Limited.
 **************************************************************************************************
*/

module.exports = {

    didServiceResp: { "did": "did:ncid:e713b9eb-6d8d-4af6-9282-c5476a3718a3" },

    authToken: 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImFwcElkLTVlMmMyMTA2LTIwYjUtNDVjYS1iNmMzLTgyOGJkNTU5NjdiZS0yMDIxLTA4LTAzVDA1OjU1OjI0LjIyIiwidmVyIjo0fQ.eyJpc3MiOiJodHRwczovL2pwLXRvay5hcHBpZC5jbG91ZC5pYm0uY29tL29hdXRoL3Y0LzVlMmMyMTA2LTIwYjUtNDVjYS1iNmMzLTgyOGJkNTU5NjdiZSIsImVcCI6MTYyOTM3NjUxNCwiYXVkIjpbIjJiNzllMzc3LTM0YmYtNDZkOS04NmZmLTZkYTMyZDFmYWQ3MSJdLCJzdWIiOiJhMTBlMzZlOC0wYTJmLTQ1ZjEtOTQyZi00YmE4MjZlOTE3MDciLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYW1yIjpbImNsb3VkX2RpcmVjdG9yeSJdLCJpYXQiOjE2MjkzNzI5MTQsInRlbmFudCI6IjVlMmMyMTA2LTIwYjUtNDVjYS1iNmMzLTgyOGJkNTU5NjdiZSIsInNjb3BlIjoib3BlbmlkIGFwcGlkX2RlZmF1bHQgYXBwaWRfcmVhZHVzZXJhdHRyIGFwcGlkX3JlYWRwcm9maWxlIGFwcGlkX3dyaXRldXNlcmF0dHIgYXBwaWRfYXV0aGVudGljYXRlZCJ9.KD4trSOwejoiptcbAKap53cLfv0u0IbdAkQyB0iofPTsJVhPqS5IPIcfwdg7fI9aLgkqio3EpW6gVkS3jFQvAuAYS5LLQOLfWJntTBCLcbPzm_vItU218b_2yuDoYLbgfsndvV6xQEGDVNGEjlVFHLsmzlEGRqmM_Z5d7yKR7dwRGiZfk6cL0nT0jg_D1jdqjy-y3wzptMA21ALOKqVCmzuCgKVWaG0HOeh-Dpr5nHbdrMxmqZUU1gJ-1HZiD5wCH7NBWG8yAs_kGQ0mMDgFPt5zRZYYZe5YsTaGrbrUg6qNTj9s9oNF8XsPJDm_vxxovFvZ3NJhqGGxKVK3dxJj8A',

    createOrgReq: {
        "roles": "Issuer",
        "name": "XYZ corp",
        "photo": "photo ID",
        "streetAddress1": "No: 11/234",
        "streetAddress2": "MLK street",
        "city": "Maryland",
        "stateProvince": "DC",
        "postalCode": "5600442",
        "countryCode": "US",
        "phone": "+1-51432-21421",
        "corpAdmin": "yes",
        "emailId": "xyzorg4@example.com"
    },

    createOrg_invalidReq: {
        "roles": "Issuer",
        "name": "XYZ corp",
        "photo": "photo ID",
        "streetAddress1": "No: 11/234",
        "streetAddress2": "MLK street",
        "city": "Maryland",
        "stateProvince": "DC",
        "postalCode": "5600442",
        "countryCode": "US",
        "phone": "+1-51432-21421",
        "corpAdmin": "yes"
    },

    createOrgRes: {
        "roles": "Issuer",
        "name": "XYZ corp",
        "photo": "photo ID",
        "streetAddress1": "No: 11/234",
        "streetAddress2": "MLK street",
        "city": "Maryland",
        "stateProvince": "DC",
        "postalCode": "5600442",
        "countryCode": "US",
        "phone": "+1-51432-21421",
        "corpAdmin": "yes",
        "emailId": "xyzorg4@example.com",
        "did": "did:ncid:e713b9eb-6d8d-4af6-9282-c5476a3718a3"
    },

    createOrgUserReq: {
        "userName": "John K13",
        "email": "johnK13@test.com",
        "phone": "1234567899",
        "orgDid": "did:ncid:7233b362-6b48-4d7f-a4e3-21acdf8a358f",
        "role": "Org Manager",
        "firstName": "John",
        "lastName": "K13",
        "password": "Passw0rd@1234"

    },

    createOrgUserRes: {
        userId: '44a00a60-18c0-4f20-8abc-58783505cb58',
        role: 'Org Manager'
    },

    listOrderUserRes: {
        "rows": [
            {
                "userId": "03d36ed9-aba5-416e-826f-8cdce31b9803",
                "orgDid": "did:ncid:cp:99a9ffee-fbce-4867-bb81-181e16b675e2",
                "email": "johnk@arv.com",
                "role": null,
                "createdAt": "2021-08-20T02:54:45.327Z",
                "updatedAt": "2021-08-20T02:54:45.327Z",
                "deletedAt": null
            },
            {
                "userId": "e3bb7431-55c9-49a0-aa44-e348cc338895",
                "orgDid": "did:ncid:7233b362-6b48-4d7f-a4e3-21acdf8a358f",
                "email": "johnS9@arv.com",
                "role": null,
                "createdAt": "2021-08-22T13:52:00.613Z",
                "updatedAt": "2021-08-22T13:52:00.613Z",
                "deletedAt": null
            }
        ],
        "count": 2
    },
    
    getUserReq: {
        params: {
            userId: 'a93d39fb-aaed-4860-8e7c-c4b40c358504'
        }
    },

    getUserRes: {
        "userId": "a93d39fb-aaed-4860-8e7c-c4b40c358504",
        "orgDid": "did:ncid:7233b362-6b48-4d7f-a4e3-21acdf8a358f",
        "email": "john0.9732883256520739@test.com",
        "role": "Org Manager",
        "createdAt": "2021-08-24T09:44:31.353Z",
        "updatedAt": "2021-08-24T09:44:31.353Z",
        "deletedAt": null
    },

    createCredDefReq: {
        "name": "string",
        "description": "string",
        "schemaId": "487ded4e-64e2-46b3-bf79-c77aa5da9c36",
        "payloadFormat": "json",
        "credentialType": "poa",
        "walletAgentId": "ncidWalletAgent"

    },

    createCredDefRes: {
        "name": "string",
        "description": "string",
        "schemaId": "487ded4e-64e2-46b3-bf79-c77aa5da9c36",
        "payloadFormat": "json",
        "credentialType": "poa",
        "walletAgentId": "ncidWalletAgent"
    },

    inCompleteCreateCredDef: {
        "name": "string",
        "description": "string",
        "schemaId": "487ded4e-64e2-46b3-bf79-c77aa5da9c36",

    },

    invalidCreateCredDef: {
        "name": "string",
        "description": "string",
        "schemaId": "487ded4e-64e2-46b3-bf79-c77aa5da9c36",
        "payloadFormat": "json",
        "credentialType": "something",
        "walletAgentId": "wrong"
    },

    getAllCredDefRes: {
        "result": [
            {
                "credentialDefinitionId": "adf8f540-a834-4e59-8939-0dd9271720c2",
                "credentialType": "poa",
                "name": "string",
                "description": "string",
                "schemaId": "487ded4e-64e2-46b3-bf79-c77aa5da9c36",
                "payloadFormat": "json",
                "walletAgentId": "ncidWalletAgent",
                "createdAt": "2021-08-30T05:50:33.885Z",
                "updatedAt": "2021-08-30T05:50:33.885Z",
                "deletedAt": null
            }
        ],
        "total": 1
    },

    getCredDefReq: {
        params: {
            credentialDefinitionId: 'adf8f540-a834-4e59-8939-0dd9271720c2'
        }
    },

    getCredDefRes: {
        "credentialDefinitionId": "adf8f540-a834-4e59-8939-0dd9271720c2",
        "credentialType": "poa",
        "name": "string",
        "description": "string",
        "schemaId": "487ded4e-64e2-46b3-bf79-c77aa5da9c36",
        "payloadFormat": "json",
        "walletAgentId": "ncidWalletAgent",
        "createdAt": "2021-08-30T05:50:33.885Z",
        "updatedAt": "2021-08-30T05:50:33.885Z",
        "deletedAt": null
    },

}
